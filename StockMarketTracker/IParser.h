// Provides interface to parse data received from REST API.
//
#pragma once

#include <string>


enum class ParserFormat
{
	JSON = 0,
	CSV,
};

class IParser
{
public:
	virtual bool Parse(const std::string& data) = 0;
	virtual bool Parse(const char* data) = 0;
};
