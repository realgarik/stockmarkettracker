#pragma once

#include "WorldTrading.h"
#include "JsonParser.h"

#include <memory>

class MarketDataHandler
{
public:
	MarketDataHandler(MarketApi restApi, ParserFormat parserFormat = ParserFormat::JSON);
	~MarketDataHandler();

	void Test();
	void SwitchParser(ParserFormat parserFormat);
	void SwitchMarketApi(MarketApi marketApi);

protected:
	std::unique_ptr<IMarketDataReciever> CreateMarketDataReceiver();
	std::unique_ptr<IJsonParser> CreateParser();

private:
	// Market API abstraction
	std::unique_ptr<IMarketDataReciever> mpMarketApi;
	MarketApi mMarketApi;

	// Parser abstraction
	std::unique_ptr<IJsonParser> mpParser;
	ParserFormat mParserFormat;
};
