#pragma once

#include <string>

enum class MarketApi
{
	WORLD_TRADING,
};

class IMarketDataReciever
{
public:
	virtual std::string StockRealTime(
		std::string symbol
		) = 0;

	virtual std::string StockIntraday(
		std::string symbol,
		int interval,
		int range,
		std::string& output
		) = 0;

private:

};

