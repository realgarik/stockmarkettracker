#pragma once

#include "IMarketDataReciever.h"
#include "Curl.h"

#include <memory>

class WorldTrading : public IMarketDataReciever
{
public:
	WorldTrading(const char* apiToken);
	~WorldTrading();

	virtual std::string StockRealTime(
		std::string symbol
		) override;

	virtual std::string StockIntraday(
		std::string symbol,
		int interval,
		int range,
		std::string& output
		) override;

private:
	std::unique_ptr<Curl> mpCurlHandler;
	std::string mApiToken;
};
