#include <iostream>
#include <sstream>
#include "Curl.h"

Curl::Curl(const char* libPath)
{
    if (CurlRet::CURL_RESULT_OK != loadLib(libPath))
    {
        throw -1;
    }
}

Curl::~Curl()
{

}

Curl::CurlRet Curl::loadLib(const char* libPath)
{
    //Curl::curlRet ret = CURL_RESULT_LIB_ERROR;

    mGetProcIDDLL = LoadLibraryA(libPath);
    if (!mGetProcIDDLL)
    {
        std::cout << "could not load the dynamic library, error: " << GetLastError() << std::endl;
        return CurlRet::CURL_RESULT_LIB_ERROR;
    }

    //
    // Initiate Curl APIs
    //
    pCurlInit = (curl_easy_init_func)GetProcAddress(mGetProcIDDLL, "curl_easy_init");
    if (!pCurlInit)
    {
        std::cout << "could not locate the curl_easy_init()" << std::endl;
        return CurlRet::CURL_RESULT_LIB_ERROR;
    }

    pCurlCleanup = (curl_easy_cleanup_func)GetProcAddress(mGetProcIDDLL, "curl_easy_cleanup");
    if (!pCurlCleanup)
    {
        std::cout << "could not locate the curl_easy_cleanup()" << std::endl;
        return CurlRet::CURL_RESULT_LIB_ERROR;
    }

    pCurlSetOpt = (curl_easy_setopt_func)GetProcAddress(mGetProcIDDLL, "curl_easy_setopt");
    if (!pCurlSetOpt)
    {
        std::cout << "could not locate the curl_easy_setopt()" << std::endl;
        return CurlRet::CURL_RESULT_LIB_ERROR;
    }

    pCurlReset = (curl_easy_reset_func)GetProcAddress(mGetProcIDDLL, "curl_easy_reset");
    if (!pCurlReset)
    {
        std::cout << "could not locate the curl_easy_reset()" << std::endl;
        return CurlRet::CURL_RESULT_LIB_ERROR;
    }

    pCurlPerform = (curl_easy_perform_func)GetProcAddress(mGetProcIDDLL, "curl_easy_perform");
    if (!pCurlPerform)
    {
        std::cout << "could not locate the curl_easy_setopt()" << std::endl;
        return CurlRet::CURL_RESULT_LIB_ERROR;
    }

    return CurlRet::CURL_RESULT_OK;
}

Curl::CurlRet Curl::Init()
{
    mCurlHandle = pCurlInit();
    if (!mCurlHandle) {
        std::cout << "CURL easy init failed..." << std::endl;
        return CurlRet::CURL_RESULT_LIB_ERROR;
    }

    return CurlRet::CURL_RESULT_OK;
}

Curl::CurlRet Curl::Uninit()
{
    pCurlCleanup(mCurlHandle);
    return CurlRet::CURL_RESULT_OK;
}

// Helper function to retrieve requested response data.
//
static size_t writeDataOnStream(void* buffer, size_t size, size_t nbytes, void* stream) {
    size_t bytes_written = fwrite(buffer, size, nbytes, (FILE*)stream);
    return bytes_written;
}


Curl::CurlRet Curl::HttpGet(const std::string& url, std::string& retData)
{
    CURLcode curlRet = CURLE_OK;
    Curl::CurlRet result = CurlRet::CURL_RESULT_OK;
    FILE* fd = NULL;

    // Prepare file to write response
    fd = fopen("foo.txt", "w+");
    if (NULL == fd)
    {
        result = CurlRet::CURL_RESULT_GENERAL_ERROR;
        goto cleanup;
    }

    // Set URL to be executed
    curlRet = pCurlSetOpt(mCurlHandle, CURLOPT_URL, url.c_str());
    if(CURLE_OK != curlRet)
    {
        std::cout << "CURLOPT_URL failed: " << /*curl_easy_strerror*/(curlRet) << std::endl;
        result = CurlRet::CURL_RESULT_LIB_ERROR;
        goto cleanup;
    }

    // Set helper function to retrieve response data
    curlRet = pCurlSetOpt(mCurlHandle, CURLOPT_WRITEFUNCTION, writeDataOnStream);
    if (CURLE_OK != curlRet)
    {
        std::cout << "CURLOPT_WRITEFUNCTION failed: " << /*curl_easy_strerror*/(curlRet) << std::endl;
        result = CurlRet::CURL_RESULT_LIB_ERROR;
        goto cleanup;
    }

    // Set file descriptor for further response data retrieval.
    // TODO: Maybe could be considered std::stream.
    // I've tied to send console output into stream, but looks like it's tricky
    // As a quick solution which is working is FILE descriptor usage.
    curlRet = pCurlSetOpt(mCurlHandle, CURLOPT_WRITEDATA, fd);
    if (CURLE_OK != curlRet)
    {
        std::cout << "CURLOPT_WRITEDATA failed: " << /*curl_easy_strerror*/(curlRet) << std::endl;
        result = CurlRet::CURL_RESULT_LIB_ERROR;
        goto cleanup;
    }

    // execute URL
    curlRet = pCurlPerform(mCurlHandle);
    if (CURLE_OK != curlRet)
    {
        std::cout << "pCurlPerform() failed: " << /*curl_easy_strerror*/(curlRet) << std::endl;
        result = CurlRet::CURL_RESULT_LIB_ERROR;
        goto cleanup;
    }    

cleanup:
    if (result == CurlRet::CURL_RESULT_OK)
    {
        // Read from file to output buffer.
        fseek(fd, 0L, SEEK_END);
        int sz = ftell(fd);

        char* buffer = new char[sz + 1]; // extra space for \0

        fseek(fd, 0, SEEK_SET);
        fread(buffer, sz, 1, fd);
        buffer[sz] = '\0';
        retData = buffer;
        delete[] buffer;
    }

    if (fd)
    {
        fclose(fd);
    }
    
    return result;
}

Curl::CurlRet Curl::HttpSet()
{
    // TODO
    return CurlRet::CURL_RESULT_OK;
}
