#include <iostream>
#include "WorldTrading.h"


WorldTrading::WorldTrading(const char* apiToken)
{
	mApiToken = apiToken;

	// TODO: Currently the DLL path is fixed, may be changed later.
	mpCurlHandler = std::make_unique<Curl>("C:\\Users\\realg\\source\\repos\\StockMarketTracker\\include\\curl\\bin\\libcurl_debug.dll");

	if (Curl::CurlRet::CURL_RESULT_OK != mpCurlHandler->Init())
	{
		throw -1;
	}
}

WorldTrading::~WorldTrading()
{
	mpCurlHandler->Uninit();
}

std::string WorldTrading::StockRealTime(
	std::string symbol)
{
	return "";
}

std::string WorldTrading::StockIntraday(
	std::string symbol,
	int interval,
	int range,
	std::string& output)
{
	std::string url = "https://intraday.worldtradingdata.com/api/v1/intraday?";

	std::string postField = "symbol=" + symbol +
		"&interval=" + std::to_string(interval) +
		"&range=" + std::to_string(range) +
		"&api_token=" + mApiToken;

	url += postField;

	mpCurlHandler->HttpGet(url, output);

	return "";
}
