#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

//#include "WorldTrading.h"
#include "MarketDataHandler.h"

#include <windows.h>
#include <iostream>


int main()
{
	std::cout << "!!! Welcome to Stock Market Tracker !!!\n" << std::endl;

	MarketDataHandler marketHandler(MarketApi::WORLD_TRADING);
	marketHandler.Test();

	return 0;

}
