#pragma once

#include "IJsonParser.h"

//const char test_data[] = "{\"symbol\":\"REI\", \"stock_exchange_short\" : \"NYSEAMERICAN\", \"timezone_name\" : \"America / New_York\", \"intraday\" : {\"2020 - 04 - 03 09:30 : 00\":{\"open\":null, \"close\" : null, \"high\" : null, \"low\" : null, \"volume\" : null}}}";

class JsonParser : public IJsonParser
{
public:
	virtual std::multimap<std::string, Value>& getParsedMap() override;

protected:
	virtual void DFSMemberCollector(const char* key, Value& value, std::multimap<std::string, Value>& mapToRet) override;
};
