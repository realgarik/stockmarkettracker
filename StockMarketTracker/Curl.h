#pragma once

#include <string>
#include <windows.h>
#include "curl/curl.h"

class Curl
{
public:

	enum class CurlRet
	{
		CURL_RESULT_OK,
		CURL_RESULT_GENERAL_ERROR,
		CURL_RESULT_LIB_ERROR,
		CURL_RESULT_INVALID_PARAM_ERROR
	};

	Curl(const char* libPath);
	~Curl();

	CurlRet Init();
	CurlRet Uninit();

	CurlRet HttpGet(const std::string& url, std::string& retData);
	CurlRet HttpSet();

protected:
	CurlRet loadLib(const char* libPath);

	typedef CURL*(__stdcall* curl_easy_init_func)();
	typedef void(__stdcall* curl_easy_cleanup_func)(CURL* curl);
	typedef CURLcode(__stdcall* curl_easy_setopt_func)(CURL* curl, CURLoption option, ...);
	typedef CURLcode(__stdcall* curl_easy_reset_func)(CURL* curl);
	typedef CURLcode(__stdcall* curl_easy_perform_func)(CURL* curl);
	

private:
	HINSTANCE mGetProcIDDLL;

	CURL* mCurlHandle;

	curl_easy_init_func		pCurlInit;
	curl_easy_cleanup_func	pCurlCleanup;
	curl_easy_setopt_func	pCurlSetOpt;
	curl_easy_reset_func	pCurlReset;
	curl_easy_perform_func	pCurlPerform;
};
