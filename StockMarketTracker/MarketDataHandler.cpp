#include "MarketDataHandler.h"

#include <iostream>

std::unique_ptr<IMarketDataReciever> MarketDataHandler::CreateMarketDataReceiver()
{
	switch (mMarketApi)
	{
		case MarketApi::WORLD_TRADING:
		{
			// TODO: API token related stuff can be modified in future to support multiple tokens.
			// Currently just fixed existing one.
			return std::make_unique<WorldTrading>("PWZLtkgEdMD8jDYlyukRQYofNeDH5HNVHxC9LIT6fGjJBRWUoC8oWCEpHiB6");
		}
		default:
		{
			return nullptr;
		}
	}
}

std::unique_ptr<IJsonParser> MarketDataHandler::CreateParser()
{
	switch (mParserFormat)
	{
		case ParserFormat::JSON:
		{
			return std::make_unique<JsonParser>();
		}
		case ParserFormat::CSV:
			std::cout << "CSV Parser is not supported..." << std::endl;
			return nullptr;
		default:
		{
			std::cout << "unsupported Parser Format..." << std::endl;
			return nullptr;
		}
	}
}

MarketDataHandler::MarketDataHandler(MarketApi restApi, ParserFormat parserFormat) :
	mMarketApi(restApi),
	mParserFormat(parserFormat)
{
	mpMarketApi = CreateMarketDataReceiver();
	std::cout << typeid(*mpMarketApi).name() << std::endl;

	mpParser = CreateParser();
	std::cout << typeid(*mpParser).name() << std::endl;
}

MarketDataHandler::~MarketDataHandler()
{

}

void MarketDataHandler::SwitchParser(ParserFormat parserFormat)
{
	mParserFormat = parserFormat;
	mpParser = CreateParser();
	std::cout << "SwitchParser: " << typeid(*mpParser).name() << std::endl;
}

void MarketDataHandler::SwitchMarketApi(MarketApi marketApi)
{
	mMarketApi = marketApi;
	mpMarketApi = CreateMarketDataReceiver();
	std::cout << "SwitchMarketApi: " << typeid(*mpMarketApi).name() << std::endl;
}

typedef struct intradaySingle_s
{
	std::string dateTime;
	double open;
	double close;
	double high;
	double low;
	uint32_t volume;
}intradayNode_t;

typedef struct intradayData_s
{
	size_t dataSize;
	intradayNode_t* data;
}intradayData_t;

void MarketDataHandler::Test()
{
	try
	{
		std::cout << "Intraday request: " << std::endl;
		// Symbol of the company which stock data going to be retrieved
		// Number of minutes between the data, 1-60
		// The number of days data is returned for, 1-30
		std::string intradayRsp;
		mpMarketApi->StockIntraday("REI", 2, 30, intradayRsp);
		std::cout << intradayRsp << std::endl;

		// Initiate internal doc with parsed string
		if (mpParser->Parse(intradayRsp.c_str()))
		{
			std::cout << "Json parse was failed..." << std::endl;
			throw -1;
		}

		std::multimap<std::string, Value>& dataMap = mpParser->getParsedMap();
	}
	catch (...)
	{
		std::cout << "Exception......" << std::endl;
	}
}
