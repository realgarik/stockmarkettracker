#include "JsonParser.h"


std::multimap<std::string, Value>& JsonParser::getParsedMap()
{
    for (Value::MemberIterator itr = mpDoc->MemberBegin(); itr != mpDoc->MemberEnd(); ++itr)
    {
        DFSMemberCollector(itr->name.GetString(), itr->value, mMappedData);
    }
    return mMappedData;
}

void JsonParser::DFSMemberCollector(
    const char* key,
    Value& value,
    std::multimap<std::string, Value>& mapToRet)
{
    switch (value.GetType())
    {
        case kNullType:
        {
            break;
        }

        case kFalseType:
        case kTrueType:
        {
            mapToRet.insert(std::pair<std::string, Value>(key, value.GetBool()));
            break;
        }

        case kStringType:
        {
            std::string strVal = value.GetString();
            mapToRet.insert(std::pair<std::string, Value>(key, (strVal, strVal.size())));
            break;
        }

        case kNumberType:
        {
            if (value.IsInt())
            {
                mapToRet.insert(std::pair<std::string, Value>(key, value.GetInt()));
            }
            else if (value.IsUint())
            {
                mapToRet.insert(std::pair<std::string, Value>(key, value.GetUint()));
            }
            else if (value.IsInt64())
            {
                mapToRet.insert(std::pair<std::string, Value>(key, value.GetInt64()));
            }
            else if (value.IsUint64())
            {
                mapToRet.insert(std::pair<std::string, Value>(key, value.GetUint64()));
            }
            else if (value.IsDouble())
            {
                mapToRet.insert(std::pair<std::string, Value>(key, value.GetDouble()));
            }
            break;
        }

        case kObjectType:
        case kArrayType:
        {
            for (Value::MemberIterator itr = value.MemberBegin(); itr != value.MemberEnd(); ++itr)
            {
                DFSMemberCollector(itr->name.GetString(), itr->value, mapToRet);
            }
            break;
        }

        default:
        {
            std::cout << "Invalid value type received..." << std::endl;
        }
    }
}

//std::string JsonParser::_getValByKey(const char* key, const char* jData)
//{
//    Document tempDoc;
//    bool keyExist = false;
//    Value* pValueForKey = nullptr;
//
//    if (jData)
//    {
//        if (tempDoc.Parse(jData).HasParseError())
//        {
//            std::cout << "failed to parse JSON data." << std::endl;
//            return "";
//        }
//
//        if (tempDoc.HasMember(key))
//        {
//            keyExist = true;
//            pValueForKey = &tempDoc[key];
//        }
//    }
//    else
//    {
//        // If input JSON data wasn't provided
//        // use initial parsed doc initiated while calling Parse() method.
//        if (mpDoc->HasMember(key))
//        {
//            keyExist = true;
//            pValueForKey = &(*mpDoc)[key];
//        }
//    }
//
//    if (keyExist && pValueForKey)
//    {
//        if (!pValueForKey->IsNull())
//        {
//            std::cout << "[ " << key << " ] found..." << std::endl;
//
//            switch ((*mpDoc)[key].GetType())
//            {
//            case rapidjson::Type::kNullType:
//                // TODO
//                return "";               
//            case rapidjson::Type::kArrayType:
//                // TODO
//                return "";
//            case rapidjson::Type::kTrueType:
//                // TODO
//                return "";
//            case rapidjson::Type::kFalseType:
//                // TODO
//                return "";
//
//            case rapidjson::Type::kNumberType:
//            {
//                std::string numStr;
//                if (pValueForKey->IsDouble())
//                {
//                    numStr = std::to_string(pValueForKey->GetDouble());
//                }
//                else if (pValueForKey->IsInt())
//                {
//                    numStr = std::to_string(pValueForKey->GetInt());
//                }
//                else if (pValueForKey->IsInt64())
//                {
//                    numStr = std::to_string(pValueForKey->GetInt64());
//                }
//
//                return numStr;
//            }
//
//            case rapidjson::Type::kObjectType:
//            {
//                // The value type is object, so convert to string and
//                // return to the caller.
//                Value& subObject = *pValueForKey;
//                StringBuffer strBuff;
//                Writer<StringBuffer> writer(strBuff);
//
//                subObject.Accept(writer);
//                std::string strToReturn = strBuff.GetString();
//                return strToReturn;
//            }
//
//            case rapidjson::Type::kStringType:
//            {
//                return pValueForKey->GetString();
//            }
//            default:
//                std::cout << "Unexpected format received." << std::endl;
//                return "";
//            }
//        }
//
//    }
//    return "";
//}
//
//std::string JsonParser::GetValByKey(const std::string& key, const char* jData)
//{
//    return _getValByKey(key.c_str(), jData);
//}
//
//std::string JsonParser::GetValByKey(const char* key, const char* jData)
//{
//    return _getValByKey(key, jData);
//}
