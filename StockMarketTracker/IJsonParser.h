#pragma once

#include "IParser.h"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include <iostream>
#include <memory>
#include <map>
#include <vector>

using namespace rapidjson;

class IJsonParser : public IParser
{
public:
	IJsonParser() { mpDoc = std::make_unique<Document>(); }
	~IJsonParser() {}

	virtual bool Parse(const std::string& jData) override
	{
		return Parse(jData.c_str());
	}

	virtual bool Parse(const char* jData) override
	{
		bool ret = mpDoc->Parse(jData).HasParseError();
		if (ret)
		{
			std::cout << "Parse error: " << mpDoc->GetParseError() << std::endl;
		}
		return ret;
	}

	virtual std::multimap<std::string, Value>& getParsedMap() = 0;

	//virtual std::string GetValByKey(const std::string& key, const char* inputData = nullptr) = 0;
	//virtual std::string GetValByKey(const char* key, const char* inputData = nullptr) = 0;

protected:
	//virtual std::string _getValByKey(const char* key, const char* jData = nullptr);
	virtual void DFSMemberCollector(const char* key, Value& value, std::multimap<std::string, Value>& mapToRet) = 0;

	std::unique_ptr<Document> mpDoc;
	std::multimap<std::string, Value> mMappedData;
};